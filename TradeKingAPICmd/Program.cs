﻿using TradeKingWrapper;
using System;
using System.Collections.Generic;

namespace TradeKingWrapperCmd
{
    class Program
    {
        const string consumerKey = "O2oH1POnW974SPjxnr2rOrxyuaZxcMxq9q4URVIkWa83";
        const string consumerSecret = "ADICHS7wclzzQKQmkrebghogFY7t9aVBoOWancTjYD80";
        const string oAuthToken = "069tV042wE6LNArKRwDTRr35E6nQS11Qc6lPY8Z9R582";
        const string oAuthSecretToken = "QRaudb8w0kfYyh8xl7l2pWYLzg3Eralst3Gqae6XImA4";

        static void Main(string[] args)
        {
            var api = new TradeKingAPI(consumerKey, consumerSecret, oAuthToken, oAuthSecretToken);
            var query = new StockQuery("MSFT");
            var details = new List<StockDetail>();
            details.Add(Args.Stock.Symbol);
            details.Add(Args.Stock.DailyHigh);
            details.Add(Args.Stock.AverageDailyPrice100);
            query.Add(details);

            var result = api.Execute(query);
            foreach (var detail in details)
            {
                Console.WriteLine("{0}: {1}", detail.Description, result[detail]);
            }
            Console.ReadLine();
        }
    }
}
